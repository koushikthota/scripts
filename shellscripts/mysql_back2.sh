#!/bin/bash

backup_parent_dir="/var/backups/mysql"

mysql_user="root"
mysql_password="227654"

if [ -z "${mysql_password}" ]; then
echo -n "ENTER MYSQL ${mysql_user} password: "
read -s mysql_password
echo
fi

echo exit | mysql --user=${mysql_user} --password=${mysql_password} -B 2>/dev/null
if [ "$?" -gt 0 ]; then
echo "MYSQL ${mysql_user} password incorrect"
exit 1
else
echo "MYSQL ${mysql_user} password correct."
fi

backup_date=`date +%Y_%m_%d_%H_%M`
backup_dir="${backup_parent_dir}/${backup_date}"
echo "Backup directory: ${backup_dir}"
mkdir -p "${backup_dir}"
chmod 700 "${backup_dir}"


 mysql_databases=`echo 'show databases' | mysql --user=root --password=227654 -B | sed /^Database$/d`

for database in $mysql_databases
do
if [ "${database}" == "information_schema" ] || [ "${database}" == "performance_schema" ];then
additional_mysqldump_params="--skip-lock-tables"
else
additional_mysqldump_params=""
fi
echo "Creating backup of \"${database}\" database"
mysqldump ${additional_mysqldump_params} --user=${mysql_user} --password=${mysql_password} ${database} | gzip > "${backup_dir}/${database}.gz"
chmod 600 "${backup_dir}/${database}.gz"
done


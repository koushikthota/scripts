#!/bin/bash

dbname=$1
username=$2
password=$3

mkdir -p /home/vagrant/dbbackups
mysqldump -u $username -p$password $dbname > /home/vagrant/dbbackups/backup.sql

result=$?

if [ $result -eq 0 ]
then
echo " the backup is successful "
else 
echo " backup failed "
fi
